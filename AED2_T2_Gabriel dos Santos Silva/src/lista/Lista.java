package lista;

import models.Node;
/*___________________________________
LSE - LISTA SIMPLESMENTE ENCADEADA
_____________________________________*/
public class Lista {
  /*_________________________________
          ATRIBUTOS DA CLASSE
  ___________________________________*/

  private int qtdNodes;

  private Node inicioLista;
  private Node finalLista;

  /*___________________________________
           CONSTRUTOR DA CLASSE
    ___________________________________*/

  public Lista() {
    inicializarLista();
  }

  /*___________________________________
          GET / SET DA CLASSE
    ___________________________________*/

  public int getqtdNos() {
    return qtdNodes;
  }

  /*___________________________________
              OEPRAÇÕES DA LSE
    ___________________________________*/
  /*___________________________________
    INICIALIZAÇÃO DA LSE
  _____________________________________*/
  private void inicializarLista() {
    inicioLista = null;
    qtdNodes = 0;
    finalLista = null;
  }
  /*___________________________________
          APAGAR A LSE
  _____________________________________*/
  public void apagarLista() {
    if (!isEmpty())
      inicializarLista();
  }
  /*__________________________________________
        VERIFICAR SE A LSE ESTÁ VAZIA
  ____________________________________________*/
  public boolean isEmpty() {
    return qtdNodes == 0;
  }
  /*___________________________________
          INSERÇÃO DE NÓ NA LSE
  _____________________________________*/
  public boolean inserir(Object objeto, int posicao) {

    if ((posicao < 0) || (posicao > qtdNodes)) {
      return false;
    } else {

      Node novoNode = new Node(objeto);

      // 1 Case: if Lista vazia.
      if (isEmpty()) {
        novoNode.setReferencia(null);

        inicioLista = novoNode;
        finalLista = novoNode;

      } else {

        // 2 Case: if posicao == 0
        if (posicao == 0) {

          novoNode.setReferencia(inicioLista);
          inicioLista = novoNode;

        } else {

          // 3 Case: if posicao == ultima posicao valida
          if (posicao == qtdNodes) {

            novoNode.setReferencia(null);
            finalLista.setReferencia(novoNode);
            finalLista = novoNode;

          } else {

            // 4 Case: Insere entre a posicao 0 e qtdNodes.
            Node referenciaAtual = inicioLista;
            for (int i = 0; i < (posicao - 1); i++) {
              referenciaAtual = referenciaAtual.getReferencia();
            }

            novoNode.setReferencia(referenciaAtual.getReferencia());
            referenciaAtual.setReferencia(novoNode);
          }
        }
      }

      // Atualiza a quantidade de nós na LSE
      qtdNodes = qtdNodes + 1;

      return true;
    }
  }
  /*___________________________________
          INSERIR NO FINAL DA LSE
  _____________________________________*/
  public boolean inserirFim(Object objeto) {

    Node novoNode = new Node(objeto);

    // 1 Case: if lista vazia.
    if (isEmpty()) {
      novoNode.setReferencia(null);

      inicioLista = novoNode;
      finalLista = novoNode;

    } else {

      novoNode.setReferencia(null);
      finalLista.setReferencia(novoNode);
      finalLista = novoNode;
    }

    qtdNodes = qtdNodes + 1;

    return true;
  }
  /*___________________________________
          REMOVER UM NÓS DA LSE
  _____________________________________*/
  public boolean remover(int p) {
    if (isEmpty() || (p < 0) || (p >= qtdNodes)) {

      return false;

    } else {

      // 1 Case: if existe apenas 1 elemento.
      if (qtdNodes == 1) {

        inicializarLista();

      } else {

        Node referenciaAtual = inicioLista;

        // 2 Case: if posicao == 0
        if (p == 0) {

          inicioLista = referenciaAtual.getReferencia();
          referenciaAtual.setReferencia(null);

        } else {

          int i = 0;

          while (i < (p - 1)) {
            referenciaAtual = referenciaAtual.getReferencia();
            i++;
          }

          // 3 Case: remover o nó da ultima posicao da LSE
          if (p == (qtdNodes - 1)) {

            referenciaAtual.setReferencia(null);
            finalLista.setReferencia(referenciaAtual);

          } else {

            // 4 Case: remover o nó entre 0 e p
            Node proximaReferencia = referenciaAtual.getReferencia();
            referenciaAtual.setReferencia(proximaReferencia.getReferencia());
            proximaReferencia.setReferencia(null);

          }
        }
      }

      qtdNodes = qtdNodes - 1;

      return true;
    }
  }

  public Object obterNoIndice(int index) {
    Node auxiliar = inicioLista;

    for (int i = 0; i < index; i++) {
      auxiliar = auxiliar.getReferencia();
    }

    return auxiliar.getObjeto();
  }
  /*________________________
        IMPRIMIR A LSE
        DADOS DO USUARIO
  __________________________*/
  public String toString() {
    String relatorio = "";

    // 1 Case: if lista !vazia significa que existe pelo menos 1 elemento na LSE.
    if (!isEmpty()) {

      Node referenciaAuxiliar = inicioLista;

      while (referenciaAuxiliar != null) {

        relatorio = relatorio + "\n" + referenciaAuxiliar.getObjeto().toString();
        referenciaAuxiliar = referenciaAuxiliar.getReferencia();
      }
    }

    return relatorio;

  }
}
