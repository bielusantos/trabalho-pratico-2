package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class Log {
  public static String infoLog = "";

  public static void save() {
    try {
      File path = new File("C:/Users/Biiel/OneDrive/Documentos/Programação/Java/AED2_T2_Gabriel dos Santos Silva/src/Logs");

      File logs[] = path.listFiles();

      String file_name = path.toString() + "\\Logs" + "-" + (logs.length + 1) + ".txt";

      FileWriter file = new FileWriter(file_name);
      BufferedWriter arq = new BufferedWriter(file);
      arq.write(infoLog);
      arq.close();

    } catch (Exception e) {
      e.getStackTrace();
    }
  }

  public static void start() {
    infoLog = "";
    Log.write("--------------------------------------------------------------");
    Log.write("                      Início da execução                      ");
    Log.write("--------------------------------------------------------------");
    Log.write("");

  }

  public static void finish() {
    Log.write("--------------------------------------------------------------");
    Log.write("                      Fim da execução                         ");
    Log.write("--------------------------------------------------------------");
    save();
  }

  public static void write(String text) {
    infoLog += text + "\n";
  }

  public static String get() {
    return infoLog;
  }
}
