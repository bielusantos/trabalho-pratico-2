package fila;

import models.User;

public class Fila {
  private User Fila[];
  private int finalFila;

  public Fila(int tamanho) {
    Fila = new User[tamanho];
    finalFila = -1;
  }

  public boolean isEmpty() {
    return finalFila == -1;
  }

  public boolean isFull() {
    return finalFila == (Fila.length - 1);
  }
  /*___________________________________
  INSERIR CONTEUDO (ENFILEIRAR - INSERIR)
  _____________________________________*/
  public boolean enqueue(String nome, String email) {

    if (!isFull()) {

      User novoNo = new User(nome, email);

      finalFila++;
      Fila[finalFila] = novoNo;

      return true;

    }

    return false; // if Fila == vazia, retorna False 

  }
  /*___________________________________
        REMOVER UM ELEMENTO DA FILA
  _____________________________________*/
  public User dequeue() {

    User resultado = front();

    if (resultado != null) {

      for (int i = 0; i < finalFila; i++) {
        Fila[i] = Fila[i + 1];
      }

      finalFila--;

    }

    return resultado;

  }
  /*___________________________________
    RETORNA A PRIMEIRA POSICAO DA FILA
  _____________________________________*/
  public User front() {
    if (!isEmpty())
      return Fila[0];
    return null;
  }
  /*___________________________________
        RETORNA O TAMANHO DA FILA
  _____________________________________*/
  public int size() {
    return (Fila.length - 1);
  }
  /*____________________
        LIMPA A FILA
  ______________________*/
  public void clear() {
    finalFila = -1;
  }
  /*_____________________________________
  PEGA A QUANTIDADE DE ELEMENTOS NA FILA
  _______________________________________*/
  public int getQuantidade() {
    return finalFila + 1;
  }

  public String toString() {

    String resultado = "";

    if (!isEmpty()) {

      for (int i = 0; i <= finalFila; i++) {
        resultado = resultado + "[ posição: " + (i + 1) + "º | " + Fila[i].getNome() + " ]\n";
      }

    }
    return resultado;
  }
}
