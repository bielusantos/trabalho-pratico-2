package pilha;

import models.Node;

public class Pilha {
  Node topo;
  int sizePilha;
  /*___________________________________
            INICIALIZA A PILHA
  _____________________________________*/
  public Pilha() {
    topo = null;
    sizePilha = 0;
  }
  /*___________________________________
        RETORNA O TAMANHO DA PILHA
  _____________________________________*/
  public int getQuantidade() {
    return sizePilha;
  }
  /*___________________________________
        VERIFICA SE A PILHA ESTA VAZIA
  _____________________________________*/
  public boolean isEmpty() {
    return topo == null;
  }
  /*_____________________
        ADICIONA A PILHA
  _______________________*/
  public void push(Object objeto) {

    Node novoNode = new Node(objeto);

    if (isEmpty()) {
      topo = novoNode;
      novoNode.setReferencia(null);
    } else {
      novoNode.setReferencia(topo);
      topo = novoNode;
    }
    sizePilha++;
  }
  /*___________________________
          REMOVE DA  PILHA
  _____________________________*/
  public Object pop() {

    Node resultado = null;

    if (!isEmpty()) {
      resultado = (Node) top();
      Node auxiliar = topo;
      topo = topo.getReferencia();
      auxiliar.setReferencia(null);
      sizePilha--;

    }
    return resultado.getObjeto();
  }
  /*___________________________________
          RETORNA O TOPO DA PILHA
  _____________________________________*/
  public Object top() {
    if (!isEmpty())
      return topo;
    return null;
  }

  public String toString() {
    String resultado = "";

    if (!isEmpty()) {
      Node auxiliar = topo;
      while (auxiliar != null) {
        resultado = resultado + auxiliar.toString();
        auxiliar = auxiliar.getReferencia();
      }
    }
    return resultado;
  }
}
