package models;

public class User {
  /*___________________________________
            ATRIBUTOS DA CLASSE
  _____________________________________*/

  private Ingresso ingresso;
  private String Nome;
  private String email;
  private String celular;

  /*___________________________________
            CONSTRUTORES DA CLASSE
  _____________________________________*/
  /*___________________________________________
  CONSTRUTOR PARA ADICIONAR UM USUARIO NA FILA
  _____________________________________________*/

  public User(String Nome, String email) {
    this.Nome = Nome;
    this.ingresso = null;
    this.email = email;
    this.celular = "";
  }
  /*_________________________________________________
  CONSTRUTOR PARAO USUARIO APÓS ADQUIRIR O INGRESSO
  ___________________________________________________*/
  public User(Ingresso ingresso, String Nome, String email, String celular) {
    this.ingresso = ingresso;
    this.Nome = Nome;
    this.email = email;
    this.celular = celular;
  }

  /*___________________________________
            MÉTODOS DA CLASSE
  _____________________________________*/

  public String getNome() {
    return Nome;
  }

  public void setIngresso(Ingresso ingresso) {
    this.ingresso = ingresso;
  }

  public String toString() {
    return "[ Nome: " + this.Nome + " | Email: " + this.email + " | Celular: " + this.celular + " | Sala: "
        + this.ingresso.getSala() + " ]";
  }

}
