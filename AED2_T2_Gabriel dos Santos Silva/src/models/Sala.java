package models;

public class Sala {
  private int numero;
  private int quantidade;
  private String filme;

  public Sala(int numero, int quantidade, String filme) {
    this.numero = numero;
    this.quantidade = quantidade;
    this.filme = filme;
  }
  /*___________________________________
      GETTERS E SETTERS PARA A SALA
  _____________________________________*/
  public int getNumero() {
    return numero;
  }

  public int getQuantidade() {
    return quantidade;
  }

  public void setQuantidade(int quantidade) {
    this.quantidade = quantidade;
  }

  public String getFilme() {
    return filme;
  }

  public String toString() {
    String info = "";

    info = "Número: " + numero + " | Filme: " + filme;

    return info;
  }
}
