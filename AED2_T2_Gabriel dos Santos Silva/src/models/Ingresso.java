package models;
  /*_______________________________________
  CRIAR INGRESSO COM DADOS DA SALA E FILME
  _________________________________________*/
public class Ingresso {
  private int sala;
  private String filme;

  public Ingresso(int sala, String filme) {
    this.sala = sala;
    this.filme = filme;
  }

  public int getSala() {
    return sala;
  }

  public String toString() {
    String info = "";
    info = "Sala: " + this.sala + " | Filme " + this.filme;
    return info;
  }
}
