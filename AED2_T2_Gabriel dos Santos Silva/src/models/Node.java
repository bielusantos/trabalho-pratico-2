package models;

public class Node {
  /*_________________________________
          ATRIBUTOS DA CLASSE
  ___________________________________*/

  private Object objeto;
  private Node referencia;

  /*__________________________________
            CONSTRUTORES DA CLASSE
  ____________________________________*/

  public Node(Object object) {
    objeto = object;
  }

  /*__________________________________
            MÉTODOS DA CLASSE
  ____________________________________*/
  /*___________________________________
            GETTERS E SETTERS
  _____________________________________*/

  public Object getObjeto() {
    return objeto;
  }

  public Node getReferencia() {
    return referencia;
  }

  public void setReferencia(Node referencia) {
    this.referencia = referencia;
  }
}
