import java.util.Random;

import fila.Fila;
import lista.Lista;
import models.Ingresso;
import models.Sala;
import models.User;
import pilha.Pilha;
import utils.Log;

public class App {

    /*___________________________________
        PARA CADA SALA CRIASSE UMA LISTA
     _____________________________________*/
  public static void inicializaSalas(Lista salas[]) {
    for (int i = 0; i < salas.length; i++) {
      salas[i] = new Lista();
    }
  }
  /*_________________________________________
  INSERE A QTD DE PESSOAS INFORMADAS NA FILA
  ___________________________________________*/
  public static void inicializaFila(Fila fila, int qtdPessoas) {
    Log.write("---- Iniciando a fila de pessoas ----");
    Log.write(" ");
    for (int i = 0; i < qtdPessoas; i++) {
      String nome = "Pessoa_" + (i + 1);
      String email = nome + "@ucl.br";

      // Insere a pessoa gerada acima na fila, if fila ta cheia, retorna False
      boolean foiAdicionado = fila.enqueue(nome, email);

      if (foiAdicionado) {
        Log.write(nome + " inserida na fila");
      } else {
        Log.write("Fila cheia! não foi possivel inserir a " + nome + " na fila");
      }
      Log.write(" ");
    }
  }

    /*________________________________________________
      GERA UMA VALOR ALEATORIO EM UM RANGE ESPECIFICO
    __________________________________________________*/
  public static int gerarValorAleatorio(int inicio, int fim) {
    return new Random().nextInt(fim) + inicio;
  }
  /*_____________________________________________________
    ADICIONA A QUANTIDADE DE INGRESSO A PILHA DE FORMA 
      ALEATÓRIA CRIANDO UM OBJETO DO TIPO INGRESSO
  _______________________________________________________*/
  public static void inicializaPilha(Pilha ingressos, int qtdIngresso, int qtdSalas, int limiteSala) {
    // LISTA  AUXILIAR PARA GERAR OS INGRESSOS
    Lista salas = new Lista();

    // REINICIALIZA A LISTA VAZIA
    salas.apagarLista();

    for (int i = 0; i < qtdSalas; i++) {
      salas.inserirFim(new Sala((i + 1), limiteSala, "Filme " + (i + 1)));
    }

    Log.write("---- Iniciando a pilha de Ingressos ----");
    Log.write(" ");

    for (int i = 0; i < qtdIngresso; i++) {

      // SE ALGO DER ERRADO NA CRIACAO DA LISTA AUXILIAR
      if (salas.isEmpty()) {
        break;
      }

      //GERA UMA VALOR ALEATÓRIO DE ACORDO COM A QUANTIDADE(0 A 5) DE SALAS
      int index = gerarValorAleatorio(0, salas.getqtdNos());
      Sala sala = (Sala) salas.obterNoIndice(index);

      Log.write("Adicionando ingresso: ( Sala " + sala.toString() + " )");
      ingressos.push(new Ingresso(sala.getNumero(), sala.getFilme()));

      sala.setQuantidade(sala.getQuantidade() - 1);

      /* SE A QUANTIDADE DE INGRESSO PRA SALA X SE ESGOTAR, REMOVE A 
          MESMA DA LISTA PARA EVITAR A SITUACAO DE SALA CHEIA*/
      if (sala.getQuantidade() == 0) {
        salas.remover(index);
        System.out.println(index);
      }
    }

    Log.write(" ");
  }
  /*___________________________________
      REALIZA A ENTREGA DE INGRESSOS
  _____________________________________*/
  public static void inicializaEvento(Lista[] salas, Pilha ingressos, Fila fila) {
    Log.write("---- Iniciando a retirada de Ingressos ----");
    Log.write(" ");

    while (true) {

      // 1 Case: acabe os ingressos
      if (ingressos.isEmpty()) {
        Log.write(" ");
        Log.write("Acabaram os ingressos!");
        Log.write("Quantidade de pessoas ainda na fila: " + fila.getQuantidade());
        break;
      }

      // 2 Case: acabe as pessoas na fila
      if (fila.isEmpty()) {
        Log.write(" ");
        Log.write("Fila vazia!");
        Log.write("Quantidade de ingressos sobrando: " + ingressos.getQuantidade());
        break;
      }

      Ingresso ingresso = (Ingresso) ingressos.pop();
      User user = fila.dequeue();
      user.setIngresso(ingresso);
      Log.write("Pessoa Entrando na sala: " + user.toString());
      int numeroSala = ingresso.getSala() - 1;
      salas[numeroSala].inserirFim(user);
    }

    Log.write(" ");
  }
  /*________________________________________________
  MÉTODO AUXILIAR PARA GERAR AS SITUACOES POSSIVEIS
  __________________________________________________*/
  public static void situacoesPossiveis(int qtdSalas, int limiteSala, int qtdPessoas) {
    System.out.println("");

    int qtdIngresso = qtdSalas * limiteSala;

    Lista salas[] = new Lista[qtdSalas];
    Pilha ingressos = new Pilha();
    Fila fila = new Fila(qtdPessoas);

    Log.start();

    inicializaSalas(salas);
    inicializaFila(fila, qtdPessoas);
    inicializaPilha(ingressos, qtdIngresso, qtdSalas, limiteSala);
    inicializaEvento(salas, ingressos, fila);

    Log.finish();
  }

  public static void main(String[] args) throws Exception {
    // 1 Case: Pessoas < Ingressos - Sobra ingressos 
    // Qtd de salas, quantidade limite da sala, qtd de pessoas
    situacoesPossiveis(5, 15, 30);
    // 2 Case: Pessoas > Ingressos: Falta ingressos
    // Qtd de salas, quantidade limite da sala, qtd de pessoas
    situacoesPossiveis(5, 10, 93);

  }
}
